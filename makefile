tag=$(shell git describe --tags | sed "s/v//g")
gitroot=$(shell git rev-parse --show-toplevel)

default: build


clean:
	@echo "Cleaning"
	rm -f $(gitroot)/dialogflow-$(tag).zip
	rm -f $(gitroot)/faas-$(tag).zip
	sleep 1
	@echo "Clean done"


pkg_dialogflow: dialogflow/entities dialogflow/intents dialogflow/agent.json dialogflow/package.json
	@echo "Package dialogflow begin"
	cat dialogflow/package.json | jq '.version |= "$(tag)"' > dialogflow/.package.json && mv dialogflow/.package.json dialogflow/package.json
	cd $(gitroot)/dialogflow && zip -r $(gitroot)/dialogflow-$(tag).zip ./
	@echo "Package dialogflow done"

pkg_faas: faas/index.js faas/package.json
	@echo "Package faas begin"
	cat faas/package.json | jq '.version |= "$(tag)"' > faas/.package.json && mv faas/.package.json faas/package.json
	cd $(gitroot)/faas && zip -r $(gitroot)/faas-$(tag).zip ./
	@echo "Package faas done"

build: clean pkg_dialogflow pkg_faas
	@echo "Build complete"