'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const { WebhookClient } = require('dialogflow-fulfillment');
const { Card, Suggestion } = require('dialogflow-fulfillment');

process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements
admin.initializeApp(functions.config().firebase);
const db = admin.firestore();

exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
  const agent = new WebhookClient({ request, response });
  console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
  console.log('Dialogflow Request body: ' + JSON.stringify(request.body));
  const userId = request.body.originalDetectIntentRequest.payload.user.userId;

  function writeToDb(agent) {
    // Get parameter from Dialogflow with the JSON to add to the database
    const remember = request.body.queryResult.parameters;

    // Get the database collection 'memories' and the user's document sub-collection for the object which they want to store
    // and store all the parameters from Dialogflow
    // The user really just wants to know the location of their item in the future, 
    // but it is useful store the verb and preposition the user used to form a more coherent sentence later

    let verb = 'have';
    let preposition = 'in';

    const document = db.collection('memories').doc(userId).collection(remember.object).doc("location");
    return db.runTransaction(t => {
      t.set(document, remember);
      return Promise.resolve('Write complete');
    }).then(doc => {
      console.log(`Wrote "${remember}" to the Firestore database.`);
      if (remember.hasOwnProperty('verb')) {
        verb = remember.verb;
      }
      if (remember.hasOwnProperty('preposition')) {
        preposition = remember.preposition;
      }

      agent.add(`I will remember that you ${verb} ${remember.object} ${preposition} ${remember.room}`);
    }).catch(err => {
      console.log(`Error writing to Firestore: ${err}`);
      agent.add(`I'm sorry I'm having trouble remembering how to remember.
                Please don't forget that you ${verb} ${remember.object} ${$preposition} ${remember.room}.
                Please try again later.`);
    });
  }

  function readFromDb(agent) {
    let memory = null;
    // Get parameter from Dialogflow with the object the user wants to find
    const remember = { object: request.body.queryResult.parameters.object };

    // Get the database collection 'memories' and the user's document sub-collection for the specified object
    const document = db.collection('memories').doc(userId).collection(remember.object).doc("location");
    // Get the value of 'room' aka the location of the object they have misplaced and send it to the user
    return document.get()
      .then(doc => {
        if (!doc.exists) {
          agent.add(`I could not find ${remember.object}.`);
        } else {
          memory = doc.data();

          let location = memory.room;
          if (Array.isArray(location)) {
            location = location.join(' ');
          }

          let verb = 'have';
          let preposition = 'in';
          if (memory.hasOwnProperty('verb')) {
            verb = memory.verb;
          }
          if (memory.hasOwnProperty('preposition')) {
            preposition = memory.preposition;
          }
          agent.add(`I remember you ${verb} ${remember.object} ${preposition} ${location}`);
        }
        return Promise.resolve('Read complete');
      }).catch(() => {
        agent.add(`I could not find ${remember.object}.`);
        agent.add('To ask me to remember where you put something, say "I left my keys in the kitchen"');
      });


  }
  // Run the proper function handler based on the matched Dialogflow intent name
  let intentMap = new Map();
  intentMap.set('Remember', writeToDb);
  intentMap.set('Where', readFromDb);
  
  agent.handleRequest(intentMap);
});
