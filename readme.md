# Mr. Remember

A Google Assistant Action that helps you remember where you left something. Built using Google Dialogflow, Clound Functions (Node.js) and Cloud Firestore.

# Installation

## Prerequisites

Install [jq](https://stedolan.github.io/jq/) command-line JSON processor
Install [zip](https://linux.die.net/man/1/zip)

Run `make build` which will create two files in the root project directory `dialogflow-1.0.0.zip` and `faas-1.0.0.zip`.

Create a Google Action Project and upload zip files accordingly.

- The dialogflow file can be uploaded to Google Dialogflow as an agent.

- The faas file can be uploaded to Google Cloud functions.

# Usage

Please visit the [homepage](https://ahmedalhulaibi.github.io/mister-remember/)

# Attribution
## Brain Icon Retrieve from Font Awesome

[License link](https://fontawesome.com/license)